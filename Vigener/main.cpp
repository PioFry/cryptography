/*
This program cyphers text from txt file with vinegre cypher
then it saves it into another txt file and splits it in columns next it calculates 
common coincidance index for user defined number of columns. it can also calculate 
coincidance index for whole text.
Please notice that code is pretty much messy, it is caused by time limit and creators' laziness.
To improve:
- Use more references instead of passing copies of vectors
- Clean up the code
- Add error messedges to prevent crashing the program with wrong input
- Try to get rid of as much variables and vectors of vectors as possible
*/



#include "std_lib_facilities.hpp"
#include <fstream>
#include <map>
#include <string>
#include <vector>



void read_from_file(std::string file_name, std::string& destination)
{
	//opening file for input(reading)
	std::ifstream file;
	file.open(file_name);
	if (!file.is_open())
	{
		cerr << "Couldn't open file";
		system("exit");
	}
	file >> destination;
	file.close();
	if (file.is_open())
	{
		cerr << "Couldn't close file";
		system("exit");
	}
}
void write_to_file(std::string file_name, std::string& source)
{
	//opening file for output(writting)
	std::ofstream file;
	file.open(file_name);
	if (!file.is_open())
	{
		cerr << "Couldn't open file";
		system("exit");
	}
	file << source;
	file.close();
	if (file.is_open())
	{
		cerr << "Couldn't close file";
		system("exit");
	}
}

vector<char> split_chars(vector<char> text, int step, int start_index)
{
	vector<char> splitted;
	for (size_t i = start_index; i < text.size(); i += step)
	{
		splitted.push_back(text[i]);
	}
	return splitted;
}

void split_columns(vector<char> text, int no_of_columns, vector<vector<char>>& columns)
{
	for (int i = 0; i < no_of_columns; i++)
	{
		columns.push_back(split_chars(text, no_of_columns, i));
	}
}

void set_map(std::map<char, int>& map)
{
	char start_sign = 97;
	for (int i = 0; i < 26; i++)
	{
		map[(start_sign + i)] = i;
	}
}

void print_map(std::map<char, int>& map)
{
	char start_sign = 97;
	int map_size = map.size();
	for (int i = 0; i < map_size - 1; ++i)
	{
		std::cout << (char)(start_sign + i) << " element: " << map[(start_sign + i)] << "\n";
	}
}

void encryption(vector<char>& text, vector<char> key, std::map<char, int> map)
{
	size_t counter = 0;
	int difference = 0;
	int value;
	for (size_t i = 0; i < text.size(); i++)
	{
		if (counter >= key.size())
			counter = 0;
		value = text[i];
		value += map[key[counter]]; /// Here we can excess max ascii number it goes back to "-127" that it why we need value as a conversion to int
		if (value > int('z')) //122 - 'z' in ascii code
		{
			difference = value - int('z');
			text[i] = (int('a') - 1) + difference;
		}
		else
		{
			text[i] = char(value);
		}
		++counter;
	}
}

vector<int> occurrences(vector<char> text, std::map<char, int> char_map, int letters)
{
	vector<int> occurence_vect(letters);
	int num_of_letters = 26;
	char first_letter = 'a';

	for (int i = 0; i < num_of_letters; ++i)
	{
		for (size_t j = 0; j < text.size(); ++j)
		{
			if (text[j] == (first_letter + i))
				occurence_vect[i] += 1;
		}
	}
	return occurence_vect;
}
double common_coin_index(vector<vector<int>> freq, int start_column, vector<vector<char>> splitted_columns, int step)
{
	int letters = 26;
	double common_coin = 0;
	double sum = 0;
	if (start_column + step >= int(freq.size()))
	{
		return 0;
	}
	for (int j = 0; j < letters; ++j)
	{
		sum += freq[start_column][j] * freq[start_column + step][j];
	}
	//according to general equation we have to do division below
	common_coin = sum / ((splitted_columns[start_column].size()) * (splitted_columns[start_column + step].size()));
	return common_coin;
}
double coincidence_index(vector<int> frequency, vector<char> text)
{
	int letters = 26;
	double coincidence = 0;
	double sum = 0;
	for (int i = 0; i < letters; ++i)
	{
		sum += frequency[i] * (frequency[i] - 1);
	}
	coincidence = sum / (text.size() * (text.size()) - 1);
	return coincidence;
}

int main()
{
	//numbers of letters in alphabet
	int num_of_chars = 26;
	//number of columns to split
	int n; 
	//step is required in common coin loop(main) to include ALL the columns common indexes
	int step = 1;
	//common_coin is required in same loop as step so that we don't check on over-range vector elements
	double common_coin = 0;
	std::string key;
	std::string text;

	//Setting dictionary so that we switch from ASCII to programist set values for chars
	std::map<char, int> char_map;
	set_map(char_map);
	print_map(char_map);

	read_from_file("tekst.txt", text);
	std::cout << "Gib key!\n";
	std::cin >> key;
	//in vector argument give begging and end iterator to get content between them.
	vector<char> text_vect(text.begin(), text.end());
	vector<char> key_vect(key.begin(), key.end());
	vector<int> frequency(num_of_chars);
	vector<vector<int>> frequencies;
	vector<vector<char>> splitted_columns;

	std::cout << "\n" << text << "\n";

	encryption(text_vect, key_vect, char_map);
	std::string encrypted_text(text_vect.begin(), text_vect.end());
	write_to_file("encrypted.txt", encrypted_text);

	std::cout << "\n" << encrypted_text << "\n";
	std::cout << "How much columns would you like to split: " << "\n";
	std::cin >> n;

	frequency = occurrences(text_vect, char_map, num_of_chars);
	split_columns(text_vect, n, splitted_columns);
	for (size_t i = 0; i < splitted_columns.size(); ++i)
	{
		frequencies.push_back(occurrences(splitted_columns[i], char_map, num_of_chars));
	}

	for (size_t j = 0; j < splitted_columns.size(); ++j)
	{
		for (size_t i = 0; i < splitted_columns.size(); ++i)
		{
			common_coin = common_coin_index(frequencies, i, splitted_columns, step);
			if (common_coin == 0)
				continue;
			else if (i == (splitted_columns.size() - 1))
			{
				std::cout << "\n\n";
				std::cout << "column " << "0" << " and " << i << ":\t";
			}
			else
			{
				std::cout << "\n\n";
				std::cout << "column " << i << " and " << (i + step) << ":\t";
			}
			std::cout << common_coin;
			std::cout << "\n\n";
		}
		step += 1;
	}

	system("pause");
	return 0;
}