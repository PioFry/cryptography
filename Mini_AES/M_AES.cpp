#include "std_lib_facilities.hpp"
#include <bitset> //bitset<>
#include <iomanip> // fill()
#include <algorithm> // reverse()


vector<string> sboxE = { "e", "4", "d", "1", "2", "f", "b", "8", "3", "a", "6", "c", "5", "9", "0", "7" };
vector<string> sboxD = { "e", "3", "4", "8", "1", "c", "a", "f", "7", "d", "9", "6", "b", "2", "0", "5" };

std::string vect_to_string(const vector<int>& vec)
{
	std::stringstream ss;
	std::string result;
	for (size_t i = 0; i < vec.size(); ++i)
	{
		ss << vec[i];
	}
	ss >> result;
	return result;
}

std::string bin_to_hex(std::string binary)
{
	std::string in_hex;
	std::stringstream ss;
	std::bitset<8> set(binary);
	ss << setfill('0') << setw(2) << hex << set.to_ulong();
	ss >> in_hex;
	return in_hex;
}

std::string hex_to_bin(std::string in_hex)	//works good if one byte is passed
{
	if (in_hex.size() != 2)
	{
		error("can convert only 8 bits at time");
	}
	unsigned int val;
	std::string in_bin;
	std::stringstream ss;
	ss << std::hex << in_hex;	//let stream know passed stuff is in hex notation
	ss >> val;	//convert hexadecimal byte into integer
	bitset<8> set(val);	//represent this integer with bits
	in_bin = set.to_string();	//return array of bits as an string
	return in_bin;
}

class Matrix
{
public:
	Matrix(std::string two_bytes);
	Matrix(bitset<4> b00, bitset<4> b01, bitset<4> b10, bitset<4> b11)
		: a00(b00), a01(b01), a10(b10), a11(b11) { }
	Matrix() : Matrix("0000000000000000") { }
	Matrix(Matrix& m) : Matrix(m.a00, m.a01, m.a10, m.a11) { }
	std::string in_hex() const;
	void print() const;
	std::bitset<4> a_00() const { return a00; };
	std::bitset<4> a_01() const { return a01; };
	std::bitset<4> a_10() const { return a10; };
	std::bitset<4> a_11() const { return a11; };
	Matrix operator+(Matrix& m);
private:
	void set_bits(std::bitset<4>& set, const std::string& init);
	std::bitset<4> a00;
	std::bitset<4> a01;
	std::bitset<4> a10;
	std::bitset<4> a11;
};

std::string Matrix::in_hex() const
{
	std::string result;
	result += bin_to_hex(a_00().to_string() + a_01().to_string());
	result += bin_to_hex(a_10().to_string() + a_11().to_string());
	return result;
}

void Matrix::set_bits(std::bitset<4>& axx, const std::string& init)
{
	int bits = 4;
	for (int i = 0; i < bits; i++)
	{
		if (init[i] == '0') {
			axx.set((axx.size()-1 - i), 0);
		}
		else if (init[i] == '1') {
			axx.set((axx.size() - 1 - i), 1);
		}
		else {
			error("Wrong bit in matrix constructor");
		}
	}
}

Matrix::Matrix(std::string two_bytes)
{
	set_bits(a00, two_bytes);		//set bits of axx to first 4 bits of two_bytes
	two_bytes.erase(two_bytes.begin(), two_bytes.begin() + 4);	//remove used bits
	set_bits(a01, two_bytes);		//set bits of axx to first 4 bits of two_bytes
	two_bytes.erase(two_bytes.begin(), two_bytes.begin() + 4);	//remove used bits
	set_bits(a10, two_bytes);		//set bits of axx to first 4 bits of two_bytes
	two_bytes.erase(two_bytes.begin(), two_bytes.begin() + 4);	//remove used bits
	set_bits(a11, two_bytes);		//set bits of axx to first 4 bits of two_bytes
	two_bytes.erase(two_bytes.begin(), two_bytes.begin() + 4);	//remove used bits
}

void Matrix::print() const
{
	std::string s00, s01, s10, s11;
	s00 = a00.to_string();
	s01 = a01.to_string();
	s10 = a10.to_string();
	s11 = a11.to_string();
	std::cout << "| " << s00 << "\t" << s01 << " |" << "\n";
	std::cout << "| " << s10 << "\t" << s11 << " |" << "\n";
}

Matrix Matrix::operator+ (Matrix& m)
{
	std::bitset<4> b00(a00 ^ m.a00);	//xor operation between operands
	std::bitset<4> b01(a01 ^ m.a01);	//xor operation between operands
	std::bitset<4> b10(a10 ^ m.a10);	//xor operation between operands	
	std::bitset<4> b11(a11 ^ m.a11);	//xor operation between operands
	std::string two_bytes;
	Matrix result(b00, b01, b10, b11);
	return result;
}

std::bitset<7> reduct_multiply(std::bitset<7> var);

void copy_bitset(std::bitset<7>& source, std::bitset<7>& dest)
{
	for (size_t i = 0; i < dest.size(); ++i)
	{
		dest[i] = source[i];
	}
}

bool is_remainder(std::bitset<7> poly)	//checks whether  poly is a remainder or not;
{
	bool remainder = true;
	for (int i = (poly.size() - 1); i >= 4; i--)
	{
		if (poly[i] == 1) //if poly has nonzero element in first 4 places (element bigger than x^3) then poly is not remainder
		{
			remainder = false;
		}
		else {}
	}
	return remainder;
}

std::bitset<7> multiply_mod2(std::bitset<4> b1, std::bitset<4> b2)
{
	std::string result_str("0000000");	//string with 7 zero elements
	std::bitset<7> result;
	for (size_t i = 0; i < b1.size(); ++i)
	{
		for (size_t j = 0; j < b2.size(); ++j)
		{
			result_str[i + j] += b1[i] & b2[j];	//and operation representing multiplication
		}
	}
	for (size_t n = 0; n < result_str.size(); ++n)
	{
		result_str[n] = (result_str[n] % 2); //modulo 2 operation though whole string
		result[n] = result_str[n];	//assign bit from string to result bitset
	}
	return result;
}

std::bitset<4> divide_mod2(std::bitset<7> poly)
{
	std::bitset<5> reduct("10011");	//in each vector position number represents power of that element 
	std::bitset<7> temporary;		// '0' means element does not exists
	std::bitset<7> to_substract;
	std::bitset<7> new_base(poly);
	std::bitset<4> result;
	//In loop below thou should think of bitset as of polymial with power of a number of place in bitset (0 if non-existant)
	for (int j = int(poly.size() - 1); j >= (reduct.size() - 1); --j)		//bitset[0] is the rightmost element
	{														//bitset[size()] access leftmost element		
		temporary[j - (reduct.size() - 1)] = new_base[j] & reduct[reduct.size() - 1];	//Division of polymial by reduct(power substraction)
		copy_bitset(reduct_multiply(temporary), to_substract); //multiplicate temp by reduct and put it in to_substract
		for (size_t i = 0; i < new_base.size(); ++i)
		{
			new_base[i] = new_base[i] ^ to_substract[i];	//xor which represents substracting polymials
		}
		temporary.reset();	//set temporary bits to zero
		if (is_remainder(new_base))	//check if resulting polymial degree is smaller than reduct degree
		{
			break;
		}
	}
	for (size_t n = 0; n < result.size(); ++n)
	{
		result[n] = new_base[n];	//copy bitsets
	}
	return result;
}

std::bitset<7> reduct_multiply(std::bitset<7> var)	//multiplication of division result with reduct poly 
{									//WARNING: var need to have only 1 non-zero bit,
									// as it is just result of highest power var division
	std::bitset<5> reduct("10011");
	std::bitset<7> result;
	for (int i = int(var.size() - 1); i >= 0; i--)	//iterate through var polymial and seek for existant element (with 1 as bit)
	{
		for (int j = int(reduct.size() - 1); j >= 0; --j)
		{
			if (var.test(i))	//if existant element is found
			{
				result[i + j] = var[i] & reduct[j]; //multiplicate it with the reduct polymial
			}
			else
			{
				break;
			}
		}
	}
	return result;
}

Matrix operator*(Matrix& m1, Matrix& m2)
{
	std::bitset<4> r00(divide_mod2(multiply_mod2(m1.a_00(), m2.a_00())) ^ divide_mod2(multiply_mod2(m1.a_01(), m2.a_10())));
	std::bitset<4> r01(divide_mod2(multiply_mod2(m1.a_00(), m2.a_01())) ^ divide_mod2(multiply_mod2(m1.a_01(), m2.a_11())));
	std::bitset<4> r10(divide_mod2(multiply_mod2(m1.a_10(), m2.a_00())) ^ divide_mod2(multiply_mod2(m1.a_11(), m2.a_10())));
	std::bitset<4> r11(divide_mod2(multiply_mod2(m1.a_10(), m2.a_01())) ^ divide_mod2(multiply_mod2(m1.a_11(), m2.a_11())));
	Matrix result(r00, r01, r10, r11);	//create new matrix
	return result;
}

Matrix zk(Matrix m)
{
	Matrix new_m(m.a_00(), m.a_01(), m.a_11(), m.a_10());
	return new_m;
}

std::bitset<4> permutation(std::bitset<4> set, vector<std::string> sbox)
{
	std::string in_hex = sbox[set.to_ulong()];
	in_hex += "0";
	std::string in_bin = hex_to_bin(in_hex);
	in_bin.erase(in_bin.end() - 4, in_bin.end());
	std::bitset<4> result(in_bin);
	return result;
}

Matrix permutation_m(Matrix m, vector<std::string> sbox)
{
	std::bitset<4> a00(permutation(m.a_00(), sbox));
	std::bitset<4> a01(permutation(m.a_01(), sbox));
	std::bitset<4> a10(permutation(m.a_10(), sbox));
	std::bitset<4> a11(permutation(m.a_11(), sbox));
	Matrix result(a00, a01, a10, a11);
	return result;
}

Matrix create_key(Matrix base_key, std::bitset<4> round_set)
{
	std::bitset<4> a00 = (base_key.a_00() ^ permutation(base_key.a_11(), sboxE) ^ round_set);
	std::bitset<4> a10 = (base_key.a_10() ^ a00);
	std::bitset<4> a01 = (base_key.a_01() ^ a10);
	std::bitset<4> a11 = (base_key.a_11() ^ a01);
	Matrix result(a00, a01, a10, a11);
	return result;
}

int main()
{
	std::bitset<4> fround_set("0001");
	std::bitset<4> sround_set("0010");
	std::string hex_text;
	std::string hex_key;
	std::string bin_key;
	std::string bin_text;
	std::string temporary;
	Matrix cyphered_text;
	Matrix m("0011001000100011");
	
	try
	{
		std::cout << "Gib text: \n";
		std::cin >> hex_text;
		std::cout << "Text in hex: " << hex_text << "\n";
		if (hex_text[0] == '0' && hex_text[1] == 'x')	//if text has 0x prefix
		{
			hex_text.erase(hex_text.begin(), hex_text.begin() + 2);	//delete this prefix
		}
		for (; hex_text.size() >= 1;)	//loop until string size is positive
		{
			if (hex_text.size() % 2 != 0)	//if string size is not multiplication of 2
			{
				hex_text.push_back('0');	//add a zero to rightmost side
			}
			temporary += hex_text[0];	//get first(leftmost) 4 bits
			temporary += hex_text[1];	//get second(leftmost) 4 bits
			bin_text += hex_to_bin(temporary);	//change those 8 bits from hex to binary notation
			hex_text.erase(hex_text.begin(), hex_text.begin() + 2);	//exclude those 8 bits from starting string
			temporary.clear();	//set capacity and size of temporary string to zero.
		}
		std::cout << "Text in binary: " << bin_text << "\n\n";
		std::cout << "Gib key: \n";
		std::cin >> hex_key;
		std::cout << "key in hex: " << hex_key << "\n";
		if (hex_key[0] == '0' && hex_key[1] == 'x')	//if text has 0x prefix
		{
			hex_key.erase(hex_key.begin(), hex_key.begin() + 2);	//delete this prefix
		}
		for (; hex_key.size() >= 1;)	//loop until string size is positive
		{
			if (hex_key.size() % 2 != 0)	//if string size is not multiplication of 2
			{
				hex_key.push_back('0');	//add a zero to rightmost side
			}
			temporary += hex_key[0];	//get first(leftmost) 4 bits
			temporary += hex_key[1];	//get second(leftmost) 4 bits
			bin_key += hex_to_bin(temporary);	//change those 8 bits from hex to binary notation
			hex_key.erase(hex_key.begin(), hex_key.begin() + 2);	//exclude those 8 bits from starting string
			temporary.clear();	//set capacity and size of temporary string to zero.
		}
		std::cout << "key in bin: " << bin_key << "\n";
		Matrix start_key(bin_key);
		Matrix text(bin_text);
		Matrix temporary;
		temporary = text + start_key;
		std::cout << "step 1: " << temporary.in_hex() << "\n";
		temporary = permutation_m(temporary, sboxE);
		std::cout << "step 2: " << temporary.in_hex() << "\n";
		temporary = zk(temporary);
		std::cout << "step 3: " << temporary.in_hex() << "\n";
		temporary = m * temporary;
		std::cout << "step 4: " << temporary.in_hex() << "\n";
		Matrix fround_key = create_key(start_key, fround_set);
		std::cout << "Frist round key: " << fround_key.in_hex() << "\n";
		temporary = temporary + fround_key;
		std::cout << "step 5: " << temporary.in_hex() << "\n";
		temporary = permutation_m(temporary, sboxE);
		std::cout << "step 6: " << temporary.in_hex() << "\n";
		temporary = zk(temporary);
		std::cout << "step 7: " << temporary.in_hex() << "\n";
		Matrix sround_key = create_key(fround_key, sround_set);
		std::cout << "Second round key: " << sround_key.in_hex() << "\n";
		temporary = temporary + sround_key;
		cyphered_text = temporary;
		std::cout << "Cyphered: " << cyphered_text.in_hex() << "\n";
		cyphered_text.print();
		system("pause");
		return 0;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
}