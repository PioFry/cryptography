#include "std_lib_facilities.hpp"
#include <cmath>
#include <math.h>
#include <limits.h>
/*
	Alot of code in this excersise is one letter variables, this is intentional as each letter is
	a copy of notation from this excersise's script
*/


// A function to print all prime  
// factors of a given number n  
vector<int> primeFactors(int n)
{
	vector<int> result;
	// Print the number of 2s that divide n
	if (n % 2 == 0)
	{
		result.emplace_back(2);
		while (n % 2 == 0)
		{
			n = n / 2;
		}
	}

	// n must be odd at this point. So we can skip  
	// one element (Note i = i +2)  
	for (int i = 3; i <= sqrt(n); i = i + 2)
	{
		// While i divides n, print i and divide n  
		while (n % i == 0)
		{
			result.emplace_back(i);
			n = n / i;
		}
	}

	// This condition is to handle the case when n  
	// is a prime number greater than 2  
	if (n > 2)
	{
		result.emplace_back(n);
	}
	return result;
}


////////////////////////////////////////////////////////////////////////////////////////////////
void delete_zeros(std::string& bits)
{
	while (bits[0] == '0')
	{
		bits.erase(bits.begin());
	}
}


template<typename T>
std::string to_binary(T val)
{
	std::size_t sz = sizeof(val)*CHAR_BIT;
	std::string ret(sz, ' ');
	while (sz--)
	{
		ret[sz] = '0' + (val & 1);
		val >>= 1;
	}
	delete_zeros(ret);
	return ret;
}

//counts base^(power) mod(modulo)
int modular_power(int base, int power, int modulo)
{
	vector<int> parts;
	std::string bits = to_binary(power);
	int p = 1;
	int temp = static_cast<int>(pow(base, p));
	int result = 1;
	temp = temp % modulo;
	parts.push_back(temp);
	p = 2;
	for (size_t i = 1; i < bits.size(); ++i)
	{
		temp = static_cast<int>(pow(temp, p));
		temp = temp % modulo;
		parts.push_back(temp);
	}
	for (size_t j = 0; j < parts.size(); ++j)
	{
		if (bits[(bits.size() - 1) - j] == '1')
		{
			result *= parts[j];
			result = result % modulo;
		}
	}
	return result;
}

bool is_primal(int root, int number)
{
	vector<int> prime_factors = primeFactors(number - 1);	
	for (size_t i = 0; i < prime_factors.size(); ++i)
	{
		if (modular_power(root, ((number -1) / prime_factors.at(i)), number) == 1)
		{
			return false;
		}
	}
	return true;
}

int count_a(int r, int k, int n)
{
	return modular_power(r, k, n);
}

vector<int> crypt(int r, int j, int n, int a, int t)
{
	vector<int> cryptogram;
	cryptogram.emplace_back(modular_power(r, j, n));
	cryptogram.emplace_back((t * modular_power(a, j, n)) % n);
	return cryptogram;
}

int decrypt(vector<int> cryptogram, int k, int n)
{
	int text = (cryptogram.at(1) * modular_power(cryptogram.at(0), n - 1 - k, n)) % n;
	return text;
}

int main()
{
	try
	{
		int n, r, k, j, t, a;
		vector<int> cryptogram;
		std::cout << "starting algorithm\n";
		std::cout << "Give n: ";
		std::cin >> n;
		std::cout << "Give r: ";
		std::cin >> r;
		if (!is_primal(r, n) || r >= (n - 1))
		{
			error("r must be < (n-1) & primal root of", n);
		}
		std::cout << "Give k: ";
		std::cin >> k;
		if (k > (n - 1))
		{
			error("k must be less than", (n-1));
		}
		a = count_a(r, k, n);
		std::cout << "Public key: " << "(" << n << ", " << r << ", " << a << ")\n";
		std::cout << "Private key: " << "(" << n << ", " << r << ", " << a << ", " << k << ")\n";
		std::cout << "Give j: ";
		std::cin >> j;
		if (j > (n - 1))
		{
			error("k must be less than", (n-1));
		}
		std::cout << "Give t: ";
		std::cin >> t;
		if (t > n)
		{
			error("t must be less than", n);
		}
		cryptogram = crypt(r, j, n, a, t);
		std::cout << "Crypted messedge: " << "(" << cryptogram.at(0) << ", " << cryptogram.at(1) << ")\n";
		std::cout << "Decryption\n";
		std::cout << "Decrypted messedge: " << decrypt(cryptogram, k, n) << "\n";



		system("pause");
		return 0;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
}