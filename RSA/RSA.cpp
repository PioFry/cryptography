#include "std_lib_facilities.hpp"
#include <cmath>
#include <math.h>
#include <limits.h>


vector<int> bool_to_int(const vector<bool>& bools);

vector<bool> int_to_bool(int n)
{
	vector<bool> result(n + 2, true); //we add 2 for zero and 1
	result.at(0) = false;
	result.at(1) = false;
	return result;
}

void erastotenes(vector<bool>& data)
{
	for (size_t i = 2; i <= sqrt(data.size() - 1); ++i)	///Add error control for data.size() = 1;
	{
		if (data.at(i))
		{
			for (size_t j = i; (i * j) < data.size(); ++j) //Here j = i eliminates repetition for examples 2*3 amd 3*2
			{
				data.at(i * j) = false;
			}
		}
	}
}

vector<int> bool_to_int(const vector<bool>& bools)
{
	vector<int> result;
	result.reserve(bools.size() / 2);
	for (size_t i = 0; i < bools.size(); ++i)
	{
		if (bools.at(i))
		{
			result.emplace_back(i);
		}
	}
	return result;
}

void print_primes(vector<int> primes)
{
	for (int i : primes)
	{
		std::cout << i << " ";
	}
}

void search_primes(const vector<int>& primaries)
{
	int a;
	for (;;)
	{
		std::cout << "which prime number would you like to see(enter non-number to stop): ";
		std::cin >> a;
		if (cin)
		{
			std::cout << a << " prime number: " << primaries[a - 1] << "\n"; //-1 because we iterate from zero!
		}
		else if (!cin)
		{
			break;
		}
	}
	cin.clear();
	cin.ignore(std::numeric_limits<streamsize>::max(), '\n');
}

struct EukRes
{
	int nwd;
	int x;
	int y;
	EukRes(int n, int xx, int yy) : nwd(n), x(xx), y(yy) { }
	EukRes() : EukRes(0, 0, 0) { }
	void print() const;
};

void EukRes::print() const
{
	std::cout << "NWD: " << nwd << "\n";
	std::cout << "X: " << x << "\n";
	std::cout << "Y: " << y << "\n";
}

EukRes euklides(int a, int b)
{
	if (a % b == 0 || b == 0)
	{
		error("can't perform euklides, division by 0");
	}
	
	vector<int> q;
	vector<int> r;
	vector<int> x;
	vector<int> y;
	//Predefine first two q's and r's
	q.emplace_back(static_cast<int>(a / b));
	r.emplace_back(a % b);
	q.emplace_back(static_cast<int>(b / r.at(0)));
	r.emplace_back(b % r.at(0));
	//Predefine first two x's and y's
	x.emplace_back(1);
	y.emplace_back(-(q.at(0)));
	x.emplace_back(-(x.at(0) * q.at(1)));
	y.emplace_back(1 - (y.at(0) * q.at(1)));
	
	for (int i = 0; r.back() != 0; ++i) // i - 2 because we already used two first iterations outside the loop
	{
		q.emplace_back(static_cast<int>(r.at(i) / r.at(i + 1)));
		r.emplace_back(r.at(i) % r.at(i + 1));
		x.emplace_back(x.at(i) - (x.at(i + 1) * q.at(i + 2)));
		y.emplace_back(y.at(i) - (y.at(i + 1) * q.at(i + 2)));
	}
	r.pop_back(); //we need r[i-1] as result
	x.pop_back(); //we need x[i-1] as result
	y.pop_back(); //we need y[i-1] as result
	EukRes result(r.back(), x.back(), y.back());
	return result;
}


void rsa(const vector<int>& primeries, int i, int j, int e)
{
	int n = primeries.at(i - 1) * primeries.at(j - 1);
	std::cout << "n: " << n << "\n";
	int m = (primeries.at(i - 1) - 1) * (primeries.at(j - 1) - 1);
	std::cout << "m: " << m << "\n";
	EukRes e_cond = euklides(e, m);
	if (e_cond.nwd != 1)
	{
		error("Error NWD(e, m) must equal 1");
	}
	int d = e_cond.x;
	while (d < 0)
	{
		d += m;
	}
	std::cout << "d: " << d << "\n";
	std::cout << "Public key: (" << n << ", " << e << ")\n";
	std::cout << "Private key: (" << n << ", " << d << ")\n";
}

int main()
{
	try
	{
		//Pure Erastotenes sieve
		std::cout << "Testing Erastotenes sieve.\n\n";
		int n;
		std::cout << "How many numbers: ";
		std::cin >> n;
		vector<int> primaries;
		std::cout << "creating bools\n";
		vector<bool> bools = int_to_bool(n);
		std::cout << "\n";
		std::cout << "Using erastotenes sieve\n";
		erastotenes(bools);
		std::cout << "\n";
		std::cout << "Converting bools to ints\n";
		primaries = bool_to_int(bools);
		std::cout << "All done!\n";
		std::cout << "\n";
		search_primes(primaries);	//enter non-number to exit
		std::cout << "Thanks for using this program\n";
		//Pure Euklides
		std::cout << "Testing Euklides.\n\n";
		int a, b;
		EukRes euklides_result;
		std::cout << "Insert a: ";
		std::cin >> a;
		std::cout << "Insert b: ";
		std::cin >> b;
		euklides_result = euklides(a, b);
		euklides_result.print();
		//RSA
		int i, j, e;
		std::cout << "Enter i: ";
		std::cin >> i;
		std::cout << "Enter j: ";
		std::cin >> j;
		std::cout << "Enter e: ";
		std::cin >> e;
		rsa(primaries, i, j, e);

		system("pause");
		return 0;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
}