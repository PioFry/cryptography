/*this program is meant to be a simplified DES (SDES) cypher.
It takes hex number as an input, (and refills it with zeros if needed)
the starting key may be random but must be 10 digits binary
and then returns hex cyphered text using first and second round key and S-boxes*/

#include "std_lib_facilities.hpp" 
#include <math.h> //pow()
#include <algorithm> //reverse() for vectors
#include <bitset> //bitset stream manipulator
#include <sstream> //stringstream
#include <string>


int bin_to_decimal(const vector<int>& binary);
//permutations
vector<int> P10 = { 2,4,1,6,3,9,0,8,7,5 };
vector<int> P4w8 = { 3,0,1,2,1,2,3,0 };
vector<int> P10w8 = { 5,2,6,3,7,4,9,8 };
vector<int> P4 = { 1,3,2,0 };
vector<int> PW = { 1,5,2,0,3,7,4,6 };
vector<int> PO = { 3,0,2,4,6,1,7,5 };
vector<int> SL1 = { 1,2,3,4,0 };
vector<int> SL2 = { 2,3,4,0,1 };
vector<int> cross = { 4,5,6,7,0,1,2,3 };
//Sboxes:
vector<vector<int>> Sbox1 = { { 1,3,0,3 },{ 0,2,2,1 },{ 3,1,1,3 },{ 2,0,3,2 } };
vector<vector<int>> Sbox2 = { { 0,2,3,2 },{ 1,0,0,1 },{ 2,1,1,0 },{ 3,3,0,3 } };

std::string bin_to_hex(const vector<int>& bin)
{
	std::stringstream ss;
	for (int bit : bin)
	{
		ss << bit;
	}
	std::string in_hex;
	ss >> in_hex;

	std::stringstream to_hex;
	std::string result;

	bitset<8> set(in_hex);
	to_hex << hex << set.to_ulong() << endl;
	to_hex >> result;
	//std::cout << "\n\n testowy vect: " << testowy << "\n";
	return result;
}

vector<int> hex_to_bin(std::string hex)
{
	vector<int> bin;
	for (size_t i = 0; i < hex.size(); ++i)
	{
		if (i == 0 && hex[i] == '0') {
			continue;
		}
		if (i == 1 && hex[i] == 'x') {
			continue;
		}
		switch (hex[i])
		{
		case '0':
			//"0000"
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(0);
			break;
		case '1':
			//"0001"
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(1);
			break;
		case '2':
			//"0010"
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(0);
			break;
		case '3':
			//"0011"
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(1);
			break;
		case '4':
			//0100
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(0);
			break;
		case '5':
			//"0101"
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(1);
			break;
		case '6':
			//"0110"
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(0);
			break;
		case '7':
			//"0111"
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(1);
			break;
		case '8':
			//"1000"
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(0);
			break;
		case '9':
			//"1001"
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(0);
			bin.push_back(1);
			break;
		case 'A':
		case 'a':
			//"1010"
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(0);
			break;
		case 'B':
		case 'b':
			//"1011"
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(1);
			bin.push_back(1);
			break;
		case 'C':
		case 'c':
			//"1100"
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(0);
			break;
		case 'D':
		case 'd':
			//"1101"
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(0);
			bin.push_back(1);
			break;
		case 'E':
		case 'e':
			//"1110"
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(0);
			break;
		case 'F':
		case 'f':
			//"1111"
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(1);
			bin.push_back(1);
			break;
		default:
			error("Invalid hexadecimal digit ", hex[i]);
		}
	}
	return bin;
}
int bin_to_decimal(const vector<int>& binary) //works checked with right input
{
	int dec = 0;
	int multiplier = 0; //we need it to read binary from right to left
	for (int i = int(binary.size() - 1); i >= 0; i--) //start at the end of vector and go to beggining
	{												  //we need i to be integer because size_t cannot go negative
		if (binary[i] == 0 || binary[i] == 1) {		  //and at final round it goes to 42555... range error...
			dec += binary[i] * narrow_cast<int>(pow(2, multiplier)); //get most-right bit and multiply 
			++multiplier;											 //with 2^multiplier
		}
		else {
			error("cannot convert non binary vector to decimal");
		}
	}
	return dec;
}

vector<int> dec_to_binary(int dec) //works checked with right input
{
	vector<int> bin;
	if (dec == 0) //CAUTION: This is extremly wrong solution for dec==0 problem, but it will do for 
	{			  //this algorithm as we know we want 2 digits binary as output (otherwise it provides undefined
		bin.push_back(0);														//behaviour)
		bin.push_back(0);
		return bin;
	}
	if (dec == 1) // for dec=1 standard loop would make only 1 round leaving 1bit binary while we want 2 bits
	{
		bin.push_back(0);
		bin.push_back(1);
		return bin;
	}
	for (int i = 0; dec > 0; ++i)
	{
		bin.push_back(dec % 2);	//get 0 or 1 into vector and divide dec by 2
		dec = dec / 2;
	}
	std::reverse(bin.begin(), bin.end()); //reverses vector order so we can read it from right to left as binary
	return bin;
}

vector<int> Sbox(const vector<int>& sbox_input, const vector<vector<int>>& sbox_pattern)
{
	//Here we do not need super long ints because we send portions of only 8 bits
	vector<int> sbox_output;
	vector<int> row_vec;
	vector<int> column_vec;
	int row = 0; //row number is a decimal made from binary of first and last element of a vector
	int column = 0; //column number is a decimal made from binary of middle part of vector (all except 1rst and last)

	row_vec.push_back(sbox_input[0]);
	row_vec.push_back(sbox_input[(sbox_input.size() - 1)]);
	row = bin_to_decimal(row_vec); //turns row_vec into decimal number

	for (size_t i = 1; i < (sbox_input.size() - 1); ++i)
	{
		column_vec.push_back(sbox_input[i]);
	}
	column = bin_to_decimal(column_vec);
	int dec_sbox = sbox_pattern[column][row];
	sbox_output = dec_to_binary(dec_sbox);
	return sbox_output;
}

vector<int> add_as_xor(const vector<int>& add_base, const vector<int>& to_add) //works checked with right input
{
	vector<int> sum;
	if (add_base.size() != to_add.size()) {
		error("addition parts sizes not equal");
	}
	for (size_t i = 0; i < add_base.size(); ++i)
	{
		if (add_base[i] + to_add[i] == 2 || add_base[i] + to_add[i] == 0) {
			sum.push_back(0);
		}
		else if (add_base[i] + to_add[i] == 1) {
			sum.push_back(1);
		}
		else {
			error("Wrong(non binary) digits in text");
		}
	}
	return sum;
}
vector<int> fuse_vect(const vector<int>& first, const vector<int>& second) //works checked with right input
{									// takes two vectors and puts it in one
	vector<int> fused;
	if (first.size() != second.size()) {
		error("halves' sizes passed to fuse not equal");
	}
	for (size_t i = 0; i < first.size(); ++i)
	{
		fused.push_back(first[i]);
	}
	for (size_t i = 0; i < second.size(); ++i)
	{
		fused.push_back(second[i]);
	}
	return fused;
}

void split_in_half(const vector<int>& base, vector<int>& first_half, vector<int>& scnd_half) //works checked with right input
{	//takes vector and splits it in two halves, then it puts it in two vectors representing halves
	first_half.clear(); //destroys all vector elements leaving it with size 0
	scnd_half.clear();
	for (size_t i = 0; i < base.size(); ++i)
	{
		if (i < (base.size() / 2)) {	//put first half digits in first vector
			first_half.push_back(base[i]);
		}
		else {
			scnd_half.push_back(base[i]); //put second half digits in second vector
		}
	}
	if (first_half.size() != scnd_half.size()) {	//check if halves are equal
		error("unequal halfs of vector");
	}
}

vector<int> permutation(const vector<int>& upper_part, const vector<int>& perm_base) //works checked with right input
{	//Makes permutation of given permutation base
	vector<int> changed;

	for (size_t i = 0; i < perm_base.size(); i++)
	{
		if (size_t(perm_base[i]) >= upper_part.size()) { // throw exception if permutation base wrongly selected
			error("permutation base calling non-exitant vector element");
		}
		changed.push_back(upper_part[perm_base[i]]);//I's digit of new vector is I's digit of old vector[perm_base]
	}
	return changed;
}

void print_vect(vector<int>& int_vect) //works checked with right input
{
	std::cout << "(";
	for (int i : int_vect)
	{
		std::cout << i << " ";
	}
	std::cout << ")" << "\n";
}

vector<int> create_key(vector<int>& fkey_half, vector<int>& skey_half, vector<int>& temporary, const vector<int>& SL_perm)
{
	vector<int> round_key;
	split_in_half(temporary, fkey_half, skey_half);
	fkey_half = permutation(fkey_half, SL_perm);
	skey_half = permutation(skey_half, SL_perm);
	temporary = fuse_vect(fkey_half, skey_half);
	round_key = permutation(temporary, P10w8);
	return round_key;
}

void round_cyphering(const vector<int>& round_key, vector<int>& temporary, const vector<int> start_text)
{
	vector<int> ftext_half, stext_half;
	vector<int> temp_first, temp_second;

	split_in_half(start_text, ftext_half, stext_half);
	temporary = stext_half;
	temporary = permutation(temporary, P4w8);
	temporary = add_as_xor(temporary, round_key);
	split_in_half(temporary, temp_first, temp_second);
	temp_first = Sbox(temp_first, Sbox1);
	temp_second = Sbox(temp_second, Sbox2);
	temporary = fuse_vect(temp_first, temp_second);
	temporary = permutation(temporary, P4);
	temporary = add_as_xor(ftext_half, temporary);
	temporary = fuse_vect(temporary, stext_half);
}

vector<int> cypher(vector<int> text, vector<int> key)
{
	//texts keys etc.
	vector<int> start_text = text;
	vector<int> start_key = key;
	vector<int> fkey_half, skey_half;
	vector<int> first_round_key;
	vector<int> second_round_key;
	vector<int> temporary;

	//first round key creation
	temporary = permutation(start_key, P10);
	first_round_key = create_key(fkey_half, skey_half, temporary, SL1);
	std::cout << "First round key:\t";
	print_vect(first_round_key); //so far so good
								 //second round key creation
	second_round_key = create_key(fkey_half, skey_half, temporary, SL2);
	std::cout << "Second round key:\t";
	print_vect(second_round_key); //also good
								  //Introducing permutation
	start_text = permutation(start_text, PW);
	//Starting first round
	std::cout << "First round: \n";
	round_cyphering(first_round_key, temporary, start_text);
	std::cout << "After 1rst round: ";
	std::cout << bin_to_hex(temporary) << "\n";
	//crossing
	temporary = permutation(temporary, cross);
	//Starting second round
	std::cout << "Second round: \n";
	round_cyphering(second_round_key, temporary, temporary);
	std::cout << "After 2nd round: ";
	std::cout << bin_to_hex(temporary) << "\n";
	//Reversed permutation
	temporary = permutation(temporary, PO);
	std::cout << "Cyphered fragment: ";
	print_vect(temporary);

	return temporary;
}

vector<int> decypher(vector<int> text, vector<int> key)
{
	//texts keys etc.
	vector<int> start_text = text;
	vector<int> start_key = key;
	vector<int> fkey_half, skey_half;
	vector<int> first_round_key;
	vector<int> second_round_key;
	vector<int> temporary;
	//first round key creation
	temporary = permutation(start_key, P10);
	first_round_key = create_key(fkey_half, skey_half, temporary, SL1);
	//second round key creation
	second_round_key = create_key(fkey_half, skey_half, temporary, SL2);
	//introducing permutation
	start_text = permutation(start_text, PW);
	//second round
	round_cyphering(second_round_key, temporary, start_text);
	//crossing
	temporary = permutation(temporary, cross);
	//round 1
	std::cout << "First round: \n";
	round_cyphering(first_round_key, temporary, temporary);
	//reversed permutation
	temporary = permutation(temporary, PO);

	return temporary;
}

void complete_vector(vector<int>& to_complete, size_t interval)
{
	int modulo = to_complete.size() % interval;
	if (modulo == 0) {
		return;
	}
	else
	{
		for (int i = 0; i < modulo; ++i)
		{
			to_complete.push_back(0); //fills vectors end with zeros, (Exercise specified to put it on end)
		}
	}
}

vector<int> extract_bits(vector<int>& source, int bits)
{
	vector<int> byte;

	if (source.size() % bits != 0) {
		error("vector size must be multiplication of number of bits to extract");
	}
	for (int i = 0; i < bits; ++i) //gets bits number of elements from argument vector's back
	{
		if (source.size() == 0) {
			error("Can't extract values from an empty vector");
		}
		byte.push_back(source.back());	//get last element of vector into byte
		source.pop_back();				//remove last element from argument vector
	}
	std::reverse(byte.begin(), byte.end());
	return byte;
}

void add_to_vector(vector<int>& source, vector<int>& destination)
{
	for (size_t i = 0; i < source.size();)	//We don't increment i 'cause source.size() is decreasing
	{
		destination.insert(destination.begin(), source.back());	//put last source element at beginning of destination
		source.pop_back();			//deletes last source element
	}
}

int main()
{
	const int bit_number = 8;
	vector<int> start_text;
	vector<int> text_temporary;
	vector<int> cyphered_text;
	vector<int> key = { 1,1,0,0,0,0,0,0,1,1 };
	std::string text_str;
	std::string cyphered;
	std::cout << "Please enter text in hexadecimal: ";
	std::cin >> text_str;

	try
	{
		//Do this only once
		start_text = hex_to_bin(text_str);
		complete_vector(start_text, bit_number);
		std::cout << "Completed text vector: ";
		print_vect(start_text);
		//repeat this
		while (start_text.size() > 0)
		{
			text_temporary = extract_bits(start_text, bit_number); //take 8 bits from the end of start_text
			std::cout << "cyphering: \n";
			text_temporary = cypher(text_temporary, key);		//cypher those 8 bits
			cyphered.insert(0, bin_to_hex(text_temporary));		//Put cyphered bits in string in hex notation	
			add_to_vector(text_temporary, cyphered_text);		//put those cyphered 8 bits in cyphered_text vector
			std::cout << "\n end of loop \n\n";
		}
		std::cout << "Result: " << cyphered << "\n";
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << "\n";
	}


	system("pause");
	return 0;
}
