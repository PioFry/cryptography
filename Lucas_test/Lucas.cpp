#include "std_lib_facilities.hpp"
#include <cmath>	//mathematics
#include <bitset>	//std::bitset<>
#include <limits.h> //CHAR_BIT	


void delete_zeros(std::string& bits)
{
	while (bits[0] == '0')
	{
		bits.erase(bits.begin());
	}
}

template<typename T>
std::string to_binary(T val)
{
	std::size_t sz = sizeof(val)*CHAR_BIT;
	std::string ret(sz, ' ');
	while (sz--)
	{
		ret[sz] = '0' + (val & 1);
		val >>= 1;
	}
	delete_zeros(ret);
	return ret;
}

class Primary
{
public:
	Primary() : val(0), t(0) { }
	void set_value(int v) { val = v; }
	void set_times(int tt) { t = tt; }
	int value() const { return val; }
	int times() const { return t; }
	void print() const;
private:
	int val;
	int t;	//krotno��
};

void Primary::print() const
{
	std::cout << "Value: " << value() << "\n";
	std::cout << "Times: " << times() << "\n";
}


int modular_power(int base, int power, int modulo)
{
	vector<int> parts;
	std::string bits = to_binary(power);
	int p = 1;
	int temp = static_cast<int>(pow(base, p));
	int result = 1;
	temp = temp % modulo;
	parts.push_back(temp);
	p = 2;
	for (size_t i = 1; i < bits.size(); ++i)
	{
		temp = static_cast<int>(pow(temp, p));
		temp = temp  % modulo;
		parts.push_back(temp);
	}
	for (size_t j = 0; j < parts.size(); ++j)
	{
		if (bits[(bits.size() - 1) - j] == '1')
		{
			result *= parts[j];
			result = result % modulo;
		}
	}
	return result;
}

vector<Primary> operator+(vector<Primary> lhs, vector<Primary> rhs)
{
	lhs.insert(lhs.end(), rhs.begin(), rhs.end());
	return lhs;
}

vector<Primary> fermath(int d)
{
	int x;
	int y2;
	int y;
	Primary result;
	vector<Primary> result_tab;

	//2nd step
	x = static_cast<int>(sqrt(d));
	double temp = sqrt(d);
	if (x == temp)
	{
		result.set_value(x);
		result.set_times(2);
		result_tab.push_back(result);
		return result_tab;
	}
	else
	{
		x += 1;
	}

	//3rd step
	while (x < (d + 1) / 2)
	{
		y2 = static_cast<int>(pow(x, 2) - d);
		temp = sqrt(y2);
		y = static_cast<int>(sqrt(y2));
		if (y2 > 0 && y == temp)
		{
			//result.set_value((x + y));
			//result.set_times(1);
			////return fermath(x + y);
			//result.set_value(x - y);
			//result.set_times(1);
			////return fermath(x - y);
			return fermath(x + y) + fermath(x - y);
		}
		else
		{
			x += 1;
		}
	}
	result.set_value(d);
	result.set_times(1);
	result_tab.push_back(result);
	return result_tab;
}

void del_repetition(vector<Primary>& vect)
{
	for (size_t i = 0; i < vect.size(); i++)
	{
		for (size_t j = 0; j < vect.size(); j++)
		{
			if (vect[i].value() == vect[j].value() && i != j)
			{
				vect[i].set_times(vect[i].times() + 1);
				vect.erase(vect.begin() + j);
			}
		}
	}
}

void fermat(int a, vector<Primary>& result_tab)
{
	//1rst step
	Primary result;
	
	
	int d = a / 2;
	result.set_value(2);
	result.set_times(1);
	int counter = 1;
	while (d % 2 == 0)
	{
		d /= 2;
		counter++;
	}
	result.set_times(counter);
	result_tab.push_back(result);

	//4th step
	result_tab = result_tab + fermath(d);
	del_repetition(result_tab);
}

void prt_primary_vec(const vector<Primary>& p_vec)
{
	for (size_t i = 0; i < p_vec.size(); ++i)
	{
		p_vec[i].print();
		std::cout << "\n";
	}
}

bool lucas(int n, int q, const vector<Primary>& primes)
{
	bool is_primary = false;
	if (modular_power(q, n - 1, n) == 1)
	{
		is_primary = true;
		for (Primary p : primes)
		{
			if (modular_power(q, (n - 1) / p.value(), n) != 1)
			{
				is_primary = true;
			}
			else
			{
				is_primary = false;
			}
		}
	}
	return is_primary;
}

int main()
{
	try
	{
		//modular_power test
		int base, power, modulo;
		std::cout << "Performing modular power test: \n\n" << "Enter base: ";
		std::cin >> base;
		std::cout << "Enter power: ";
		std::cin >> power;
		std::cout << "Enter modulo: ";
		std::cin >> modulo;
		std::cout << "Modular power result:  " << modular_power(base, power, modulo) << "\n";

		//fermat test
		std::cout << "Fermat and Lucas: \n\n";
		int n, q;
		std::cout << "Enter n: ";
		std::cin >> n;
		std::cout << "Enter q: ";
		std::cin >> q;
		vector<Primary> n_primes;
		fermat(n, n_primes);
		std::cout << "n primaries: \n";
		prt_primary_vec(n_primes);

		//lucas test
		if (lucas(n, q, n_primes))
		{
			std::cout << "n is a primary!\n";
		}
		else
		{
			std::cout << "Test does not resolve data for n\n";
		}

		system("pause");
		return 0;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
}